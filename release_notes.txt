	4.4.0 Update (31.10.21) Release Notes

 - Double click effect of Save Form button in MR, PO forms fixed;
 - Adding MIF number into Lumb Sum/Remarks column implemented;
 - Find and Replace function improoved. Added search by column function;
 - Code base cleaned and refactored. More fast Log In as effect;
 - GPM module improoved and refactored;
 - Rows Number textbox and show last added row by default added into MR Form view;
 - Program crash on MR Form items adding fixed;
	
	4.3.1 Update (25.10.21) Release Notes

 - MR Form permission for Originator from HSSE department changed;
 - Temporary directory on updade check fixed;
 - System Log file check fixed;
 	
	4.3.0 Update (20.10.21) Release Notes

 - Production Map for Piping Citeria Filter 'Joint' field multiple selection fixed;
 - Program error log system improved;
 - Program update process improved;
 	
	4.2.0 Update (12.10.21) Release Notes

 - Program core code base full update;
 - Clear Filters (column filters) button added in all register views (except Doc Control);
 - Piping Criteria Filters modifed;
  	
	4.1.0 Update (22.09.21) Release Notes

 - Database model updated;
 - Fixed add/edit/remove MR items from Form;
 - Fixed LS removal on PO Unapproved;
		
	4.0.3 Update (16.09.21) Release Notes

 - Row style (red background) for removed records fixed;
 - Lumb Sum records removal on PO removal/exclude implemented;
 - Lumb Sum 'Procurement Quantity' check on new data export fixed;
 - Saving data on new records adding logic changed;
 - PO Form 'Vendor' checkbox content bug after form update fixed;
 	
	4.0.2 Update (11.09.21) Release Notes

 - Fixed Lumb Sum register 'Received Quantity' check message on new added records.
 - Fixed MR Form items sort after removal items from list while user creates new MR.
 	
	4.0.1 Update (08.09.21) Release Notes

 - Fixed PO Form 'Vendor' checkbox content in View mode.
 - Added Lumb Sum register 'Received Quantity' check message on data update.
	
	4.0.0 Update (05.09.21) Release Notes

 - Global Database update. 'Piping' module added.
 - Data verification on save implemented.
 - Fixed minor UI problems.
 	
	3.6.13 Update (03.08.21) Release Notes

 - PO Form procurement users permissions changed;
 - PO Form Unapproved functionality added;
	
	3.6.11 Update (14.06.21) Release Notes

 - PO Form: Updated PO Form print template;
		
	3.6.10 Update (12.06.21) Release Notes

 - MR Form: Fixed Material Quantity on partial PO creation;
 - PO Form: Fixed bug of MR Form records loss on exclusion PO records from PO Form;
 - PO Form: MR data update on using 'Include' implemented;
 - MR Register: MR and MR Form 'Item Number' sort on records removal from MR registry implemented;
	
	3.6.9 Update (08.06.21) Release Notes
	
 - Fixed Lumb Sum register data update on excluded PO items;
 - Fixed Lumb Sum 'Delivery Quantity', 'Delivery Date' data update on material transfer to stocks;
 - Minor UI corrections;
	
	3.6.8 Update (26.05.21) Release Notes
	
 - Date style changed to "dd-mm-yyyy" like style in MR and PO form EXLS templates (print);
 - Fixed 0 (zero) value bug of Amount in PO Registry on PO Form creation;
 - Fixed/modifed user settings on records removal from MR Form;
	
	3.6.7 Update (22.05.21) Release Notes
	
 - Material Management/Material Issue Form modified. Lumb Sum Register "Electon Sign" copies data to the Forms "E/Q" column.
 - Some main window buttons view changed.
 	
	3.6.6 Update (18.05.21) Release Notes
	
 - Items removal from MR Form bug fixed;
 - Adding new items to MR Form fixed. 
 - Items numbers sorting fixed on removal/adding items in MR Form and MR Register;
 - New currency available (GBP, TRY) on PO Form creation;
	
	3.6.5 Update (16.05.21) Release Notes
	
 - Fixed MR Form update bug;
 - Fixed MR register record update on MR Form update;
	
	3.6.4 Update (15.05.21) Release Notes
	
 - Handeled errors on MR and PO Forms creation (minimized program crash on unckown errors);
 - PO Status check improved on MR Form creation;
 - MR, PO Register records edit desabled by default (now user can enable it in Program Settings);
	
	3.6.3 Update (07.05.21) Release Notes
	
 - Fixed Lumb Sum records creation on new PO (wrong item numbers on partial MR);
 - Fixed Lumb Sum records update on PO items exclusion;
 - Fixed update MR Form data on MR Register update;
	
	3.6.2 Update (06.05.21) Release Notes
	
 - Fixed and improved adding partial materials into PO. 
 - Fixed Procurement Quantity value in Partial Lumb Sum table (in particular of partial PO);
 - Fixed and improved Partial PO items removal (for Excluded items not implemented yet).
 - Merge all canceled MR records into one record on Partial PO items removal implemented;
 - Fixed logic on multible PO records removal;
 - Fixed PO`s 'Total' value calculation on saving with Excluded items;
 - Improved PO Number check on new PO creation (question dialog if new number is not set);
 - PO Form checkboxed set to 'inactive' on new PO creation (for security reasons);
 - Fixed some minor logic problems;
	
	3.6.1 Update (01.05.21) Release Notes
	
 - Fixed/improved Partial Lumb Sum table saving (Quantity on Stock values update);
	
	3.6.0 Update (28.04.21) Release Notes
	
 - New function added to PO Form. Now Procurement users can "Exclude" items in the material list.
 Lumb Sum Register data automatically updates. Items marks as Deleted (in Red color);
	
	3.5.7 Update (05.04.21) Release Notes
	
 - Fixed saving user`s default MR Form settings;
	
	3.5.6 Update (26.03.21) Release Notes
	
 - Fixed loading Units in MR Form window`s table in View Mode;
	
	3.5.5 Update (25.03.21) Release Notes
	
 - Fixed program error on saving MR Form, that was restored from *.mrf file;
 - Changed MR Number check algorithm on saving MR Form;
	
	3.5.4 Update (23.03.21) Release Notes
	
 - Purchase Order Form: Fixed update Po Date on Form data update;
 - Dates view corrected in PO and MR Forms;
 - Fast Import function settings saving fixed;
 - User Account settings (administrative): Fixed duplicates in user group members on press Apply.
	
	3.5.2 Update (10.03.21) Release Notes
	
 - PO Form Originator name changing on PO update fixed;
 - Lumb Sum table Filter Clear buttons fixed;
 - Some messages on Task Bar behavior modifed;
 - All Filters code base refactored;
	
	3.5.1 Update (01.03.21) Release Notes
	
 - Update MR Date bug on new PO Form creation fixed;
 - View of Date on PO Form modified;
	
	3.5.0 Update (28.02.21) Release Notes
	
 - Using server date/time on data creation implemented;
 - Incorrect Date on new MR Form creation fixed;
	
	3.4.4 Update (26.02.21) Release Notes
	
 - MR Form permissions for Procurement users changed;
 - Information messages added to Material Management register on MR register upating on save;
 - Logic of incorrect Delivery Date in PO Form changed. Question message added;
 - Bug on PO Form saving fixed;
 - Pending data logic modified;
 - MR Date on MR Form data update fixed;
 - EUR currency added to Procurement module;
 - PO Form items limit bug fixed;
 - MR data rollback on PO removal implemented;
 - Lumb Sum register data update on PO saving as Approved modified;
	
	3.4.0 Update (20.02.21) Release Notes	
	
 - Filter for PO registry table added;
 - Saving MR Form to *.mrf file fixed/impoved;
 - Form open buttons in Main Window activity fixed;
	
	3.3.2 Update (17.02.21) Release Notes	
	
 - Add/Remove items in MR Form fixed/improved;
 - Update MR items in Registry list when MR Form updated fixed;
 - PO Form creation errors handled;
 - Fixed MR update on Lumb Sum Registry items (with not partial quantity) update;
	
	3.3.0 Update (15.02.21) Release Notes
	
 - New Filter added for MR table;
 - PO Number generation modified;
 - PO AmountAZN calculation fixed;
 - MR Form permission changed for Procurement Users;
	
	3.2.1 Update (09.02.21) Release Notes
	
 - Fixed updating MR Form data;
 - Fixed saving PO Form to XLS file;
 - Fixed updating PO Form data;
	
	3.2.0 Update (08.02.21) Release Notes
	
 - Material Requisition register model changed. New column 'Package' added;
 - MR Number generation on new MR Form creation modified (MR No check moved to the end of creation process);
 - Saving MR Form to it`s own format (if MR is not saved to the Database) implemented. 
 - Copy/Paste function implemented to MR Form. Only for 'Description' column;
 - New 'Fast Import' function added to "Import from Excel file" function. Data copies diretly to the main table.
 - MR Form some 'Position' cells set as editable.
 - Fixed filers "Nothing was found" message;
 - Fixed register number of rows on new MR creation;
 - Some columns in MR registed made as 'hidden' for non Procurement Department users;
 - Fixed Pending Lists search algorithm;
 - Fixed ROS Date data saving to XLS file;
	
	3.1.0 Update (01.02.21) Release Notes
	
 - New table (Lumb Sun with loined Partial Quantity) aded (as read only). Filter for LS table used;
 - Fixed PO register data update on PO Form data update;
 - Adding new Partial LS record logic modified/improved;
 - Partial LS recod`s 'MRNo', 'ItemNo' types changed to string;
	
	3.0.6 Update (18.01.21) Release Notes
 
 - Creation MR Form modified (Unit comboboxes added to the table, Originator row added);
 - Fixed PO Form cration minor problems;
 - PO File freation modified;
 - Handled PO file cration whet PO Form not saved;
 
	3.0.4 Update (09.01.21) Release Notes
 
 - Fixed import from Excel file for PO table;
 - Fixed columns order and Details textboxes in PO table view;
 
	3.0.3 Update (05.01.21) Release Notes
 
 - Import from MS Excel file data type errors handeld;
 - Reading single quotes (') in MS Excel file data handled;
 - Fixed long loading MR register;
 
	3.0.2 Update (01.01.21) Release Notes
 
 - MRF Saving to file permissions modified;
 - Fixed User Account Settings Add/Remove Groups, table permissions, User Group Memmbers (Admin);
 - MRF`s table 'Unit' values check added;
 - User Table permissions list modified (Admin);
 
	3.0.1 Update (22.12.20) Release Notes
	
 - System Log fully implemented;
 - Fixed DateTime data in System Log; 
 - Fixed LS/PRO Tables materials Received Quantity to stocks transfer;
 - Fixed clearing table columns filters on loading full table data;
	
	3.0.0 Update (18.12.20) Release Notes	
	
 - Program side bar design changed;
 - 'Find and Replace' function improved. Column selection added.
 - Tools buttons panel now the same for all tables;
 - Selection requests logic changed in program core;
 - Fixes some bugs in Materaials Requisition Form logic;
	
	2.0.2 Update (12.11.20) Release Notes
	
 - Opening Materaials Requisition Form by Responsible Person fixed;
	
	2.0.1 Update (11.11.20) Release Notes
	
 - SQL Server user type check on Loging changed (given the specifics of the new users posts).
	
	2.0.0 Update (03.11.20) Release Notes
	
 - Purchase Order Form verify order changed.
 - Adding Vendor information to the file footer in PO Form implemented;
 - MR Number generation and checking in MR Form fixed;
 - Saving program update files information to the System Registry inplemented;
 - Showing Program version on user pressed 'Not Now' button in Update Dialog fixed;
	
	1.9.9.0 Update (27.10.20) Release Notes
	
 - Material Service Requisition Form saving to file fully implemented;
 - Purchase Order Form saving to file fully implemented;
 - Fixed program behavior on searching empty text or whitespaces;
 - Checkig MR number on Import function added;
 - Fixed Filters behavior on user SQL Server permissions errors;
 - Fixed generation PO number on PO Form creation;
 - Fixed Partial Resieved Quantity in Project Materials register;
 - Fixed null DataTime in MR Form, Po Form;
 - Fixed number of rows textbox on register table clearing;
	
	1.9.8.0 Update (27.10.20) Release Notes
	
 - MR tables`s Details pane`s textboxes corrected, old removed;
 - Fixed bug in transfer materials to SWHCE stock;
 - Fixed some SQL Server permissions bugs;
	
	1.9.7.0 Update (26.10.20) Release Notes
 
 - User loging to SQL Server mechanism changed.
 - Fixed some problems in Filters.
 
	1.9.6.0 Update (22.10.20) Release Notes
	
 - Fixed using Unicode characters in Filters (such as 'ə', 'ş' etc.)
	
	1.9.5.0 Update (22.10.20) Release Notes
	
 - Procurement Management module updated and changed.
 - Impoved Filter dialogs added to Lumb Sum and Project Materaials tables.
 - Saving data to the Data Base in the program core changed.
 - Material Issue Form templates limit improved to 100.
 - Many little visual changes in the program.
 - Added some UI features for user confort work.
 - Fixed some program bugs.
 
	1.9.4.0 Update (12.07.20) Release Notes
	
 - New three tables added to Document Control.
 - Added 'Marked' counter to Project Materials and Lumb Sum Tables.
 - Added 'Added to MIF' counter to Project Materials and Lumb Sum Tables.
 - Added Force Update option in Program Prefereces.
 - Fixed some bugs of program logic.
	
	1.9.3.0 Update (30.06.20) Release Notes
	
 - Fixed ROS Date message in Material Requisition Form creation process.
 - MR and PO file templates replaced into Application folder (used by default).
 - Program Update window corrected.
	
	1.9.2.0 Update (29.06.20) Release Notes
	
 - Fixed bug in Document View function.
 - Fixed saving table data after using Search function. 
	
	1.9.1.0 Update (22.06.20) Release Notes
	
 - Procurement Section functionaltiy modified and improved.
 - Improved saving process on SQL Server connection lost.
 - A lot of options added to Program Prefereces.
 - Dashboard tab added to save Log Out process.
 - A lot of bugs fixed.
 
	1.9.0.0 Update (02.01.20) Release Notes
	
 - Material Requisiton Form logic added;
 - User Control System globally updated;
 - Status bar added to main window;
 - Fixed asynchroneus work of modal windows (Login Window also);
 - Visual changes in Main Window and Detais pane in every table view;
 - Visual changes in some dialog windows;
 - Fixed some bugs;
 
	1.7.0.5 Update (26.10.19) Release Notes:
 
 - Fixed bug on adding new record in a table;
 - Fixed showing waring dialog on program exit;
 - Fixed dialog on table switching;
 
	1.7.0.4 Update (23.10.19) Release Notes:
 
 - Fixed bug of checking user rights in MIF table;
 - Fixed saving "Check On Empty Record" setting in Program Preferecies;
 
	1.7.0.0 Update (22.10.19) Release Notes:
 
 - Globally updated messaging system;
 - Added new messages (such as Question Message);
 - Moved some buttons to Main Window;
 - Some functions moved (such as Utilites) to Main Window menu bar;
 - Fixed saving marked records;
 - Fixed saving existing data;
 - Fixed dublicated on Import Data list while saving marked items;
 - Fixed bug in Row Settings Window;
 - Fixed Open Specific file type dialog windos (file types was not filtered);
 - Fixed bug in MIF table after Depleted Message was shown;
 
	1.6.9.1 Update (15.10.19) Release Notes:
 
 - Fixed opening Import Row Settings dialog window;
 
	1.6.9.0 Update (14.10.19) Release Notes:
 
 - Fixed bug on opening program settings (program was crushing on opening sum tables);
 
	1.6.8.0 Update (11.10.19) Release Notes:
 
 - Checking for dublicate recods added to PRO and LS "Add to MIR" function;
 - Handled situation of various types adding to MIR;
 - Fixed savid Materials Issue Form when Dynamic Substitution function is on;
 - Added checking for maximum of MIR records while using Add To MIR function;
 
	1.6.7.0 Update (09.10.19) Release Notes:
 
 - Added template Dynamic Substitution function in Material Issue Form settings;
 - Fixed bug in saving Settings in Program Prefereces;
 - Added Document Location fields in Project Materials and Lumb Sum tables;
  
	1.6.6.0 Update (08.10.19) Release Notes:
 
 - Saving Material Issue Form Log table state implemented;
 - Fixed bug in saving records marked as "Not Saved";
 - Fixed bug of Load/Update button in Document Control section;
 - Fixed bug of Material Depleted message after deleteing items from Material Issue Form;
 - Added setting rows number and default template`s sheet name for Material Issue Form in Program Prefereces;
 - Added manual "Issued Quantilty Check" utility in "Project Materaials" and "Lumb Sum" tables;
 
	1.6.0.0 Update (04.10.19) Release Notes:
 
 - Added new table "PO";
 - Created new tab. "MR" and "PO" tables moved there;
 - Improved saving data in all tables to new algorithm;
 - Added order numbers in Material Issue Form table (automatic and manual);
 - "Form Issued By" column added to MIF log table;
 - Fixed opening ".mif" files in Material Issue Form table;
 - Fixed some omissions in program code.
 
	1.5.5.0 Update (27.09.19) Release Notes:
 
 - Improved Material Issue Form number control while saving Form;
 - Added automatic cloning function of "From" and "To" fields in Material Issue Form;
 - Cloning of "From" and "To" fields option added to the Program Prefereces;
 - Fixed updating Material Issue Form table state after Material Depleted error occures;
 - Fixed saving of existing records;
 
	1.5.0.0 Update (25.09.19) Release Notes:
 
 - Added stock checking function in Material Issue Form (automatic and manual);
 - Improved Depleted materials checking in Material Issue Form;
 - Added cloning function of "From" and "To" coulmns values in Material Issue Form;
 - Changed buttons view in Material Management section;
 
	1.4.3.0 Update (23.09.19) Release Notes:
 
 - Optimized saving algorithm in Project Materials and Lumb Sum tables;
 - Added "Update after saving data" setting in Program Preferences;
 - Added "Atomatic load statistics" setting in Program Preferences;
 - Fixed Error message on pressing Load/Update button;
 
	1.4.0.0 Update (20.09.19) Release Notes:
 
 - Administration tab removed;
 - Material Issue Log moved to Material Management tab;
 - Added buttons activity state on the Tools Panel, depeds of table type;
 - Corrected rounding in some values in Project Management and Lumb Sum;
 
	1.3.0.0 Update (19.09.19) Release Notes:
 
 - Added Table Filter to Lump Sum table (for test);
 - Fixed deleting records from Material Issue Form table;
 
	1.2.0.0 Update (13.09.19) Release Notes:
 
 - Search bar moved to Tools tables Tool Panel;
 - Fixed bugs on swithcing tables;
 - Removed Jump To Last Row button (which had some problems);
 - Removed top lable panel (with logo);
 
	1.1.0.0 Update (11.09.19) Release Notes:
 
 - Statistics added to Project Materials and Lumb Sum Tables;
 
	1.0.2.0 Update (10.09.19) Release Notes:
 
 - Fixed Error Messages after using Search function;
 - Expander added to Search Texbox;
 - Search Texbox expanded to 900 points;
 
	1.0.0.0 Update (10.09.19) Release Notes:
 
 - Global visual changes in program main tabs and side buttons;
 - Material Management section moved to it`s own Tab;
 - Deleted Search Tab;
 
	0.29.24.0 Update (09.09.19) Release Notes:
 
 - Fixed bugs in Material Issue Form logic;
 - Added 1 new column in MIF;
 
	0.29.23.0 Update (06.09.19) Release Notes:
 
 - Fixed and changed Project Materials and Lumb Sum tables logic;
 - Improved paste process from buffer in tables (now use can see paseted data);
 - Fixed Paste Marked rows function;
 - Changed diesign of Project Materials and Lumb Sum panes;
 - Fixed saving to Database bugs;
 - Fixed Database Concurrancy errors;
 
	0.29.21.0 Update (03.09.19) Release Notes:
 
 - Material Issue Form transactions bugs fixed;
 - Fixed appying table settings after pressing Jump Last Row button;
 - Material Issue Form template changed (20 rows added);
 - Main Window hot keys changed (see tooltips on buttons);
 
	0.29.19.0 Update (02.09.19) Release Notes:
 
 - Clearing filters added to Load/Update button (by default all filters cleared when table updated);
 
	0.29.18.0 Update (31.08.19) Release Notes:
 
 - Lump Sum Table Changed;
 - Added some buttons shortcuts in Main Window (see tooltips of buttons);
 - Fixed some bugs;
 
	0.29.16.0 Update (29.08.19) Release Notes:
	
 - Lump Sum Table Changed;
 - Fixed some bugs;
 
	0.29.13.0 Update (24.08.19) Release Notes:
 
 - Fixed user unauthorized access to the system;
 - Modified GPM View (Admins only);
 - Add Empty Row function now creates rows with empty DataTime type;
 - Handled some tables operations errors;
 
 	0.29.12.1 Update (22.08.19) Release Notes:
 
 - Fixed user Unauthorized Access Error;
 
	0.29.12.0 Update (20.08.19) Release Notes:
 
 - "Please wait..." and "Caps Lock is On" messages added to Login Window;
	
	0.29.11.0 Update (19.08.19) Release Notes:
	
 - Jump To Last Row button added;
 - Row Selection Settings added to Program Prefereces;
 
 	0.29.9.0 Update (17.08.19) Release Notes:
	
 - Added automatically opening Import Tab after using Import function;
 - Removed unused button in Main Menu;
 - Fix some program behavior bugs;
 
	0.29.8.2 Update (15.08.19) Release Notes:
 
 - Main Menu reorginized and some buttons names changed;
 - Two new tables added;
 - Project Materials and Lump Sum Materials tables logic improved;
 - Material Issue Form table logic improved;
 - Safe/Open From File function added to Material Issue Form;
 - Material Management settings added to Program Preferences;
 - Import Settings added to Program Prefereces;
 - Improvement in Import from Excel file functionaltiy;
 - Little changes in program messages;
 - Some bugs in program logic fixed;

 
	0.27.5.9 Update (01.08.19) Release Notes:
 
 - "Project Materials" and "Lump Sum Materials" fully updated;
 - Added "Material Issue Form" fuctionality (logic and creating .xlsx (Excel) file);
 - Added transfer logic for "Project Materials" table;
 - Added transfer logic for "Lump Sum Materials" table;
 - Application Settings now saves in System Registry;
 - Added Template Directory settings;
 - Added Save Last Loging settings;
 - Added "Material Issue Form" settings;
 - View logs of "Material Issue Form" added (in Administrative Tab);
 - Fixed some Database Error messages;
 - Fixed some application logic bugs;
 - Login Window design changed;
 - Little visual changes in Details pane of tables;
 
	0.21.4.5 Update (11.06.19) Release Notes:
 
 - Fixed QSL Server Settings;
 - Fixed development code base;
 
	0.22.4.2 Update (09.07.19) Release Notes:
 
 - AZFEN Procedures/Specifications table changed
 - Changes in AZFEN Procedures/Specifications in Details pane;
 
	0.22.0.0 Update (08.07.19) Release Notes:
 
 - AZFEN Procedures/Specifications table Details changed;
 - User Data (Login and Password) function added;
 
	0.21.9.0 Update (03.07.19) Release Notes:
 
 - Fixed bug of calculating rows number after using filter;
 - Fixed work of filters;
 - Corrected view of Statistics Pane, colors changed;
 
	0.21.7.0 Update (02.07.19) Release Notes:
 
 - About Program window added;
 - Program version checking direcory changed;
 - Full Material Issue Form saving to form inplemented;
 - Azfen Procedures/Specifications table changed, removed some unused coulmns;
 - Fixed "Find and Replase" function. Now user can replace any part of text.
 - Fixed some other bugs;
 
	0.21.1.0 Update (23.06.19) Release Notes:
 
 - Added message about dublicates;
 - Fixed bug of pasting records from buffer;
 - Fixed bug of deleting just added records;
 - Fixed bug of "Import Successfully" message even if type of table is not correct;
 - Fixed update table after trying to add existing items now user can see the red lines;
 - Fixed simultaneous editing the same records by various users;
 - Improved saving process in tables;
 - "Project Materials", "Lump Sum Materials" and "Material Issue Form" tables added;
 - Fixed showing message bug about saving if user already saved table;
 - Material Issue Form work logic implemented;
 
	0.17.1.2 Update (11.06.19) Release Notes:
 
 - Fixed some unallowable bugs;
 
	0.17.1.1 Update (03.06.19) Release Notes:
 
 - Fixed bug of deleting marked (in the system) as Not Saved records.
 - Corrected Program behavior when user checks Program Update while data is not saved.
 
	0.17.0.0 Update (01.06.19) Release Notes:
 
 - Export to Excel file function added to all tables.
 
	0.16.5.2 Update (01.06.19) Release Notes:
 
 - Fixed bug in business calculations in "Project Materials" table.
 - Note, Note2 and Note3 colunmns in "Project Materials" table now Editable. 
      (user now can add his own information in "Details" pane).
 
	0.16.5.0 Update (31.05.19) Release Notes:
 
 - Increased security settings of user table permissions of Details Pane TextBoxes;
 
	0.16.4.1 Update (30.05.19) Release Notes:
 
 - Fixed some bugs in User Statistics;
 - User table security permissions modified in "Import from MS Excel" function;
 
	0.16.2.0 Update (29.05.19) Release Notes:
 
 - Added color trigger to all tables. Not Saved records fonts colored in red;
 - Fixed bug of deleting records from database;
 
	0.16.0.0 Update (27.05.19) Release Notes:
 
 - Added two new tables to Material Management section: Project Materials, Lump Sum Meterials
 - Removed "Azf099" Table;
 - Added "Find and Replace" function;
 - Changed background to coloured in "Details" window, Text Boxes corrected;
 - Fixed bug of SQL Server access;
 - Fixed bug of checking update config file;
 - Fixed bug of deleting not saved record;
 - User table permissions added;
 